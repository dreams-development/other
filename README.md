# Other
## Terminal
### Console
- [Colorize Your Shell Commands without any libraries or plugins](https://codeburst.io/colorize-your-shell-commands-without-any-libraries-or-plugins-2cd4a264a17e)
- [7 Super Useful Aliases to make your development life easier](https://codeburst.io/7-super-useful-aliases-to-make-your-development-life-easier-fef1ee7f9b73)
- [How to Jazz Up Your Bash Terminal — A Step By Step Guide With Pictures](https://medium.freecodecamp.org/jazz-up-your-bash-terminal-a-step-by-step-guide-with-pictures-80267554cb22)
- [Jazz Up Your “ZSH” Terminal In Seven Steps — A Visual Guide](https://medium.freecodecamp.org/jazz-up-your-zsh-terminal-in-seven-steps-a-visual-guide-e81a8fd59a38)
- [Консоль в массы. Переход на светлую сторону. Часть вторая](https://habrahabr.ru/post/318376/)
- [Bash-скрипты: начало](https://habr.com/company/ruvds/blog/325522/)
- [Become a Command Line Ninja With These Time-Saving Shortcuts](https://lifehacker.com/5743814/become-a-command-line-ninja-with-these-time-saving-shortcuts)
### Tmux
- [Docs](http://man.openbsd.org/tmux)
- [Basic tmux Tutorial - Windows, Panes, and Sessions over SSH](https://youtu.be/BHhA_ZKjyxo?list=WL) (in video description sorted commands)
- [Basic tmux Tutorial, Part 2 -- Shared Sessions](https://www.youtube.com/watch?v=norO25P7xHg)(video)
- [The Tao of tmux](https://tmuxp.git-pull.com/en/latest/about_tmux.html)

## Vim
- [Understanding Vim’s Jump List](https://medium.com/@kadek/understanding-vims-jump-list-7e1bfc72cdf0)
- [You don’t need more than one cursor in vim](https://medium.com/@schtoeffel/you-don-t-need-more-than-one-cursor-in-vim-2c44117d51db)
- [Git merge with vim](https://gist.github.com/karenyyng/f19ff75c60f18b4b8149)
- [Learn Vimscript the Hard Way](http://learnvimscriptthehardway.stevelosh.com/)

